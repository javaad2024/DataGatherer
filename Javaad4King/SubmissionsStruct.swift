//
//  SubmissionsStruct.swift
//  Javaad4King
//
//  Created by AdvocatesClose on 5/17/24.
//

import Foundation


struct Submissions: Codable {
		//	let id = UUID()
	var cik: String = "34088"
	var entityType: String = "operating"
	var sic: String = "2911"
	var sicDescription: String = "Petroleum Refining"
	var insiderTransactionForOwnerExists: Int = 1
	var insiderTransactionForIssuerExists: Int = 1
	var name: String = "EXXON MOBIL CORP"
	var tickers: [String] = []
	var exchanges: [String] = []
	var ein: String = "135409005"
	var description: String = ""
	var website: String = ""
	var investorWebsite: String = ""
	var category: String = "Large accelerated filer"
	var fiscalYearEnd: String = "1231"
	var stateOfIncorporation: String = "NJ"
	var stateOfIncorporationDescription: String = "NJ"
	var addresses: Addresses = Addresses()
	var phone: String = "9729406000"
	var flags: String = ""
	var formerNames: [FormerName] = []
	var filings: Filings = Filings()
}

struct Filings: Codable {
	var recent: Recent = Recent()
}

struct Recent: Codable {
	var accessionNumbers: [String] = [String]()
	var filingDate: [String] = [String]()
	var reportDate: [String] = [String]()
	var acceptanceDateTime: [String] = [String]()
	var act: [String] = [String]()
	var form: [String] = [String]()
	var fileNumber: [String] = [String]()
	var filmNumber: [String] = [String]()
	var items: [String] = [String]()
	var size: [Int] = []
	var isXBRL: [Int] = []
	var isInlineXBRL: [Int] = []
	var primaryDocument: [String] = [String]()
	var primaryDocDescription: [String] = [String]()
}

struct FormerName: Codable, Hashable {
	var name: String = "name"
	var from: String = "from"
	var to: String = "to"
}

struct Addresses: Codable, Hashable {
		//	let id = UUID()
	var mailing: Address = Address()
	var business: Address = Address()
}

struct Address: Codable, Hashable, Identifiable {
	let id = UUID()
	var street1: String = "123 Sesame Street"
	var street2: String? = "Apt 27G"
	var city: String = "New York"
	var stateOrCountry: String = "New York"
	var zipCode: String = "10029"
	var stateOrCountryDescription: String = "10029"
}

extension Submissions {
	func buildSubmissionsList() -> [SubmissionsClass] {
		var submissionsClass = [SubmissionsClass]()
		for i in 0..<self.filings.recent.accessionNumbers.count {
			if self.filings.recent.act[i] == "33" || self.filings.recent.act[i] == "34" {
				let submission = SubmissionsClass(accessionNumber: self.filings.recent.accessionNumbers[i].replacingOccurrences(of: "-", with: ""), form: self.filings.recent.form[i], items: self.filings.recent.items[i])
				submissionsClass.append(submission)
			} else {
				continue
			}
		}
		return submissionsClass
	}
}

	
struct SubmissionsClass {
	var accessionNumber: String
	var form: String
	var items: String
	
	init(accessionNumber: String = "", form: String = "", items: String = "") {
		self.accessionNumber = accessionNumber
		self.form = form
		self.items = items
	}
}

