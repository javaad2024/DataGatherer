//
//  QueryResponseStruct.swift
//  Javaad4King
//
//  Created by AdvocatesClose on 5/19/24.
//

import Foundation




struct QueryResponseContainer: Codable {
	var total: Total = Total()
	var queryResponses: [QueryResponse] = [QueryResponse]()
	
	private enum CodingKeys: String, CodingKey {
		case total
		case queryResponses = "filings"
	}
}

struct QueryResponse: Codable, Hashable, Identifiable {
	var id: String = "idString"
	
	var accessionNo: String = "number-yy-numbers"
	var filedAt: String = "June, 47, 1451"
	var formType: String = "411"
	var items: [String]? = []
	var description: String? = ""
	var periodOfReport: String? = ""
	
	var cik: String = "1234"
	var ticker: String = "ABCX"
	var companyName: String = "ABC Corporation"
	
	
	var linkToFilingDetails: String = ""
	var linkToTxt: String = ""
	var linkToHtml: String = ""
	var linkToXbrl: String = ""

	var entities: [Entity]? = []

	var documentFormatFiles: [Attachment] = []
	var dataFiles: [Attachment] = []
}

struct Entity: Codable, Hashable, Identifiable {
	let id = UUID()
	var cik: String? = ""
	var irsNo: String? = ""
	var companyName: String? = ""
	var stateOfIncorporation: String? = ""
	var fiscalYearEnd: String? = ""
	var sic: String? = ""
	var type: String? = ""
	var act: String? = ""
	var fileNo: String? = ""
	var filmNo: String? = ""
	
	private enum CodingKeys: String, CodingKey { case cik, irsNo, companyName, stateOfIncorporation, fiscalYearEnd, sic, type, act, fileNo, filmNo }
}

struct Attachment: Codable, Hashable, Identifiable {
	let id = UUID()
	var sequence: String? = "sequence"
	var description: String? = "description"
	var documentUrl: String = "documentUrl"
	var type: String? = "type"
	var size: String? = "size"
	
	private enum CodingKeys: String, CodingKey { case id, sequence, description, documentUrl, type, size }
}


