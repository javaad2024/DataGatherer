	//
	//  RegistrationStatementSearchView.swift
	//  Javaad4King
	//
	//  Created by AdvocatesClose on 5/18/24.
	//

import SwiftUI
import TabularData

struct RegistrationStatementSearchView: View {
	
	@State private var queryResponse = RegistrationStatementsContainer()
	@State private var selection = RegistrationStatement()
	
	var queryResponses: [RegistrationStatement] {
		return queryResponse.registrationStatements
	}
	
	
	
	var body: some View {
		NavigationStack {
			List(selection: $selection) {
				ForEach(queryResponses, id: \.accessionNo) { filing in
					NavigationLink {
						VStack {
							Text("\(filing.cik.removeLeadingZeros())")
							Link("GO", destination: URL(string: "https://www.sec.gov/Archives/edgar/data/\(filing.cik.removeLeadingZeros())/\(filing.accessionNo.replacingOccurrences(of: "-", with: ""))/index.json")!)
							
							
							
						}
					} label: {
						Text(filing.ticker)
					}
				}
			}
		}
		.navigationTitle(" ")
		.toolbar {
			Button(action: {
				Task {
					do {
						queryResponse = try await fetchData()
					} catch {
						print(error.localizedDescription)
					}
				}
			}, label: {
				Text("Fetch Data")
			})
		}
	}
	
	
	func fetchData() async throws -> RegistrationStatementsContainer {
		let urlToEndpoint = URL(string: "https://api.sec-api.io/form-s1-424b4")!
		var request = URLRequest(url: urlToEndpoint)
		request.httpMethod = "POST"
		request.setValue("application/json", forHTTPHeaderField: "Content-Type")
		request.addValue("cba221f705b9631078b3cb797bfa90607d9ff2fc06c95a7a13c8bad01e1f22a0", forHTTPHeaderField: "Authorization")
		let queryDict = [
			"query": "(formType:424*) AND lawFirms.name:\"Davis Polk*\"",
			"from": "0",
			"size": "50"
		]
		
		let jsonEncoder = JSONEncoder()
		jsonEncoder.outputFormatting = .prettyPrinted
		let jsonData = try jsonEncoder.encode(queryDict)
		request.httpBody = jsonData
		
		let (data, response) = try await URLSession.shared.data(for: request)
		let str = String(decoding: data, as: UTF8.self)
		print(str)
		let registrationStatementContainer = try JSONDecoder().decode(RegistrationStatementsContainer.self, from: mapResponse(response: (data: data, response: response)))
		
		return registrationStatementContainer
	}

}

#Preview {
	RegistrationStatementSearchView()
}


