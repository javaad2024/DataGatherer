//
//  ResultDetail.swift
//  Javaad4King
//
//  Created by AdvocatesClose on 5/24/24.
//

import SwiftUI


struct QueryResponseDetail: View {
	var queryResponse: QueryResponse

	var body: some View {
		NavigationStack {
			QueryResponseDetailHeaderView(queryResponse: queryResponse)
			ScrollView {
				AccessionNumberContainerView(queryResponse: queryResponse)
			}
		}
		.padding()
	}
}



struct ReportedItem: Codable, Identifiable {
	let id = UUID()
	var item: String = ""
	var description: String = ""
	
	enum CodingKeys: String, CodingKey { case id, item, description }
}

struct QueryResponseDetailHeaderView: View {
	var queryResponse: QueryResponse
	
	var body: some View {
		HStack {
			VStack(alignment: .leading) {
				Text(queryResponse.companyName)
					.font(.largeTitle)
				Text(queryResponse.ticker)
					.font(.title3)
			}
			Spacer()
			VStack(alignment: .trailing) {
				Text("Form \(queryResponse.formType)")
					.font(.title)
					.fontWeight(.bold)
				Text("Filed \(queryResponse.filedAt)")
					.font(.body)
			}
		}
	}
}

#Preview {
	QueryResponseDetail(queryResponse: QueryResponse())
}
