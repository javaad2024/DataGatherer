//
//  UnusedObjects.swift
//  Javaad4King
//
//  Created by AdvocatesClose on 5/25/24.
//

import Foundation
import SwiftUI

func makeUrlRequestToSecApi(url urlEndpoint: URL) -> URLRequest {
	var request = URLRequest(url: urlEndpoint)
	
	request.httpMethod = "POST"
	
	request.addValue("Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/17.5 Safari/605.1.15", forHTTPHeaderField: "User-Agent")
	
	let queryDict = ["User-Agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/17.5 Safari/605.1.15"]
	
	let jsonEncoder = JSONEncoder()
	jsonEncoder.outputFormatting = .prettyPrinted
	let jsonData = try? jsonEncoder.encode(queryDict)
	request.httpBody = jsonData
	return request
}


private struct AlignmentLabelStyle: LabelStyle {
	var alignment: HorizontalAlignment
	
	func makeBody(configuration: Configuration) -> some View {
		if alignment == .trailing {
			HStack {
				configuration.title
				configuration.icon
			}
		} else {
			HStack {
				configuration.icon
				configuration.title
			}
		}
	}
}
