//
//  StringFormattingExtensions.swift
//  Javaad4King
//
//  Created by AdvocatesClose on 5/17/24.
//

import Foundation

extension String {
//	func formatStringDate() -> Date {
//		
////		var dateFormatter = ISO8601DateFormatter()
////		let newDate = dateFormatter.date(from: date)
////		dateFormatter.setLocalizedDateFormatFromTemplate("MMM d, yyyy")
////		guard let newDate = newDate else { return "Date" }
////		outDate = dateFormatter.string(from: newDate)
////		return outDate
//	}

	func thePhone(_ number: String) -> String {
		var outPhone = String()
		let format = /(\d\d\d)(\d\d\d)(\d\d\d\d)/
		if let match = number.firstMatch(of: format) {
			outPhone = "+1 (\(match.1)) \(match.2)-\(match.3)"
		}
		return outPhone
	}
	
	
	func hyphenateAccessionNumber() -> String {
		var returnString: String = ""
		let accessionNumber = /(\d{10})(\d{2})(\d{,10})/
		
		if let match = self.firstMatch(of: accessionNumber) {
			returnString = "\(match.1)-\(match.2)-\(match.3)"
		}
		
		return returnString
	}
	
	func removeLeadingZeros() -> String {
		var returnString: String = ""
		if self.count == 10 {
			print("The CIK is \(self)")
			let leadingZeros = /0+([1-9]\d+)/
			if let match = self.firstMatch(of: leadingZeros) {
				print("Match: \(match.1)")
				returnString = String(match.1)
			}
		} else {
			returnString = self
			print("no match")
		}
		return returnString
	}
}
