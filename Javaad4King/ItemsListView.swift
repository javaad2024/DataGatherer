//
//  ItemsList.swift
//  Javaad4King
//
//  Created by AdvocatesClose on 5/25/24.
//

import SwiftUI

struct ItemsListView: View {
	var result: QueryResponse
	@State private var items = [ReportedItem]()
	
	var body: some View {
		VStack(alignment: .leading) {
			HStack {
				Text("Items")
					.font(.title2)
				Spacer()
			}
			
			ForEach(items, id: \.id) { item in
				HStack(alignment: .top) {
					Text(item.item)
						.font(.headline)
					Spacer()
					Text("\(item.description)")
						.font(.headline)
				}
			}
		}
		.task {
			items = buildItems(result: result)
		}
	}
	func buildItems(result res: QueryResponse) -> [ReportedItem] {
		var outArray = [ReportedItem]()
		let pattern = #"(.+):(.+)"#
		let regex = try? NSRegularExpression(pattern: pattern)
		guard let items = res.items else { return [] }
		for i in 0..<items.count {
			let str = items[i]
			let matches = regex?.matches(in: str, range: NSRange(location: 0, length: str.utf16.count))
			guard let matches = matches else { return [] }
			for match in matches {
				let firstHalfRange = match.range(at: 1)
				let item = (str as NSString).substring(with: firstHalfRange)
				let secondHalfRange = match.range(at: 2)
				let description = (str as NSString).substring(with: secondHalfRange)
				let reportedItem = ReportedItem(item: item, description: description)
				outArray.append(reportedItem)
			}
		}
		return outArray
	}
}

#Preview {
	ItemsListView(result: QueryResponse())
}
