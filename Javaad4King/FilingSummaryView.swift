////
////  FilingSummaryView.swift
////  Javaad4King
////
////  Created by AdvocatesClose on 5/25/24.
////
//
//import SwiftUI
//
//
//struct FilingSummaryView: View {
//	var queryResponse: QueryResponse
//	
//	@State private var documentsListedInFilingSummary = [DocumentListedOnFilingSummary]()
//	
//	
//	var body: some View {
//		NavigationStack {
//			VStack(alignment: .leading) {
//				makeSectionHeading(withTitle: "Filing Summary")
//				
//				ForEach(documentsListedInFilingSummary, id: \.id) { item in
//					Text("Hello")
//					makeFilingView(for: item)
//				}
//			}
//		}
//		.task {
//			do {
//				documentsListedInFilingSummary = try await getMatches(result: queryResponse)
//				
//			} catch {
//				print("Error at filing summary view")
//			}
//		}
//	}
//	func makeSectionHeading(withTitle t: String) -> some View {
//		HStack {
//			Text("\(t)")
//				.font(.title2)
//			Spacer()
//		}
//	}
//	
//	func makeFilingView(for item: DocumentListedOnFilingSummary) -> some View {
//		HStack(alignment: .top) {
//			Text("\(item.filename)")
//				.font(.subheadline)
//			Spacer()
//			Text("\(item.filename)")
//				.font(.subheadline)
//		}
//	}
//	
//	func makeUrlRequestToSecApi(url urlEndpoint: URL) -> URLRequest {
//		var request = URLRequest(url: urlEndpoint)
//		request.httpMethod = "POST"
//		request.addValue("Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/17.5 Safari/605.1.15", forHTTPHeaderField: "User-Agent")
//		let queryDict = ["User-Agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/17.5 Safari/605.1.15"]
//		let jsonEncoder = JSONEncoder()
//		jsonEncoder.outputFormatting = .prettyPrinted
//		let jsonData = try? jsonEncoder.encode(queryDict)
//		request.httpBody = jsonData
//		return request
//	}
//	
//	func makeUrlToFilingSummary(result res: QueryResponse) -> URL {
//		let urlString = "https://www.sec.gov/Archives/edgar/data/\(res.cik)/\(res.accessionNo.replacingOccurrences(of: "-", with: ""))/\(res.accessionNo)-index-headers.html"
//		let url = URL(string: urlString)!
//		let urlWithCik = url.appending(path: res.cik)
//		let urlWithAccessionNumber = urlWithCik.appending(path: res.accessionNo.replacingOccurrences(of: "-", with: ""))
//		let urlToFilingSummary = urlWithAccessionNumber.appending(path: "FilingSummary.xml")
//		print("\(urlToFilingSummary)")
//		return urlToFilingSummary
//	}
//	
//	func getMatches(result res: QueryResponse) async throws -> [DocumentListedOnFilingSummary] {
//		let url = makeUrlToFilingSummary(result: queryResponse)
////		let urlToFilingSummary = URL(string: urlString)!
//		let str1 = try String(contentsOf: url)
//
//		print(str1)
//		let pattern = #"&lt;([A-Z]+)&gt;\n?(.+)(\n\n.*?)?"#
//		
//		var i = 0
//		var documentsArray = [DocumentListedOnFilingSummary]()
//		do {
//			let regex = try NSRegularExpression(pattern: pattern, options: [.allowCommentsAndWhitespace])
//			
//			var documentListedOnFilingSummary = DocumentListedOnFilingSummary()
//			
//			let matches: Void = regex.enumerateMatches(in: str1, range: NSRange(location: 0, length: str1.utf16.count), using: { match, flags, stop in
//				guard let match = match else { return }
//				
//				
//				let firstMatchRange = match.range(at: 1)
//				let substringForFirstMatch = (str1 as NSString).substring(with: firstMatchRange)
//				let secondMatchRange = match.range(at: 2)
//				let substringForSecondMatch = (str1 as NSString).substring(with: secondMatchRange)
//				
//				
//				i += 1
//				var j: Int
//				j = i % 5
//				
//				func buildDocForSummary(_ j: Int) -> [DocumentListedOnFilingSummary] {
//					switch j {
//						case 1:
//							documentListedOnFilingSummary.document = substringForSecondMatch
//						case 2:
//							documentListedOnFilingSummary.sequence = substringForSecondMatch
//						case 3:
//							documentListedOnFilingSummary.filename = substringForSecondMatch
//						case 4:
//							documentListedOnFilingSummary.description = substringForSecondMatch
//						case 0:
//							documentListedOnFilingSummary.text = substringForSecondMatch
//							documentsArray.append(documentListedOnFilingSummary)
//							print(documentListedOnFilingSummary)
//							documentListedOnFilingSummary = DocumentListedOnFilingSummary()
//						default:
//							print("error")
//					}
//					return documentsArray
//				}
//				documentsArray = buildDocForSummary(j)
//				
//				print("\(i).\t\(substringForFirstMatch): \(substringForSecondMatch)\tFirst Match:\(firstMatchRange), Second Match:\(secondMatchRange)\n")
//			})
//		} catch {
//			print("nothing")
//		}
//		return documentsArray
//	}
//}
//
//#Preview {
//	FilingSummaryView(queryResponse: QueryResponse())
//}
