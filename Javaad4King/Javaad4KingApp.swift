//
//  Javaad4KingApp.swift
//  Javaad4King
//
//  Created by AdvocatesClose on 5/14/24.
//

import SwiftUI

@main
struct Javaad4KingApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
		.windowStyle(.hiddenTitleBar)
    }
}
