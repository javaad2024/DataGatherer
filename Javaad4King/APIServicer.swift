//
//  APIServicer.swift
//  Javaad4King
//
//  Created by AdvocatesClose on 5/27/24.
//

import Foundation


class APIServicerMan {
	
	struct APIResponse: Sequence {
		let start: Int
		
		func makeIterator() -> APIResponseIterator {
			return APIResponseIterator(self)
		}
	}
	
	struct APIResponseIterator: IteratorProtocol {
		let response: APIResponse
		var times = 0
		
		init(_ response: APIResponse) {
			self.response = response
		}
		
		mutating func next() -> Int? {
			let nextNumber = response.start - times
			guard nextNumber > 0
			else { return nil }

			times += 1
			return nextNumber
		}
	}
	
	func composeURLRequest () -> URLRequest {
		let urlToEndpoint = URL(string: "https://api.sec-api.io")!
		var request = URLRequest(url: urlToEndpoint)
		request.httpMethod = "POST"
		request.setValue("application/json", forHTTPHeaderField: "Content-Type")
		request.addValue("cba221f705b9631078b3cb797bfa90607d9ff2fc06c95a7a13c8bad01e1f22a0", forHTTPHeaderField: "Authorization")
		let queryDict = [
			"query": "formType:\"10-K\"",
			"from": "0",
			"size": "50"
		]
		
		let jsonEncoder = JSONEncoder()
		jsonEncoder.outputFormatting = .prettyPrinted
		let jsonData = try? jsonEncoder.encode(queryDict)
		request.httpBody = jsonData
		return request
	}
	
	func fetchData<T>(withRequest request: URLRequest, fromService name: String) async throws -> T where T : Codable {
		let request = makeURLRequestForJansQueryApi()
		let (data, response) = try await URLSession.shared.data(for: request)
			//		print(response)
		let container = try JSONDecoder().decode(T.self, from: mapResponse(response: (data: data, response: response)))
		print(data)
		return container
	}
	
	func makeURLRequestForJansQueryApi() -> URLRequest {
		let urlToEndpoint = URL(string: "https://api.sec-api.io")!
		var request = URLRequest(url: urlToEndpoint)
		request.httpMethod = "POST"
		request.setValue("application/json", forHTTPHeaderField: "Content-Type")
		request.addValue("cba221f705b9631078b3cb797bfa90607d9ff2fc06c95a7a13c8bad01e1f22a0", forHTTPHeaderField: "Authorization")
		let queryDict = [
			"query": "formType:\"10-K\"",
			"from": "0",
			"size": "50"
		]
		
		let jsonEncoder = JSONEncoder()
		jsonEncoder.outputFormatting = .prettyPrinted
		let jsonData = try? jsonEncoder.encode(queryDict)
		request.httpBody = jsonData
		return request
	}
}
//https://gist.githubusercontent.com/princefishthrower/30ab8a532b4b281ce5bfe386e1df7a29/raw/5bfd40048667f406bb5c704efe58cd087ae9a81f/sandp500.json
