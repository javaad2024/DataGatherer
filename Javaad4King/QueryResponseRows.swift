//
//  QueryResponseRow.swift
//  Javaad4King
//
//  Created by AdvocatesClose on 5/29/24.
//

import SwiftUI

struct QueryResponseRows: View {
	var queryResponses: [QueryResponse]
	
	@State private var selectedResponse: QueryResponse?
	
    var body: some View {
		List {
			ForEach(queryResponses, id: \.id) { queryResponse in
				NavigationLink {
					QueryResponseDetail(queryResponse: queryResponse)
				} label: {
					QueryResponseResultRow(queryResponse: queryResponse)
				}
			}
		}
    }
}

#Preview {
	QueryResponseRows(queryResponses: [QueryResponse]())
}
