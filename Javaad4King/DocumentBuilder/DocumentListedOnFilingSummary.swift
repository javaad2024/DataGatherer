//
//  DocumentListedOnFilingSummary.swift
//  Javaad4King
//
//  Created by AdvocatesClose on 5/25/24.
//

import Foundation

struct DocumentListedOnFilingSummary: Codable, Hashable, Identifiable {
	let id = UUID()
	var document: String = "default"
	var sequence: String = "default"
	var filename: String = "default"
	var description: String = "default"
	var text: String = "default"
	
	enum CodingKeys: String, CodingKey {
		case id
		case document = "DOCUMENT"
		case sequence = "SEQUENCE"
		case filename = "FILENAME"
		case description = "DESCRIPTION"
		case text = "TEXT"
	}
}


