//
//  DocumentClass.swift
//  Javaad4King
//
//  Created by AdvocatesClose on 5/26/24.
//

//import SwiftUI
import Cocoa
import WebKit


class MyDocument: NSDocument {
	var webView: WKWebView!
	
	override init() {
		do {
			let documentString = try String(contentsOf: URL(string: "https://www.sec.gov/Archives/edgar/data/1944048/000162828023006327/index.json")!, encoding: .utf16)
		} catch let error as NSError {
			Swift.print(error.localizedDescription)
		}
		super.init()
	}
	
	override class var autosavesInPlace: Bool {
		return true
	}
	
	override func makeWindowControllers() {
		let windowController = NSWindowController(windowNibName: NSNib.Name("MyDocument"))
		self.addWindowController(windowController)
	}
	
	override func windowControllerDidLoadNib(_ windowController: NSWindowController) {
		super.windowControllerDidLoadNib(windowController)
		
		webView = WKWebView(frame: windowController.window!.contentView!.bounds)
		windowController.window!.contentView!.addSubview(webView)
		webView.autoresizingMask = [.width, .height]
	}
	
	override func data(ofType typeName: String) throws -> Data {
			// Return the document's data as an NSData object.
			// Encode your document with an instance of NSData or NSFileWrapper.
		return Data()
	}
	
	override func read(from data: Data, ofType typeName: String) throws {
			// Load your document from data.
	}
	
	func fetchHTMLContent(from url: String) {
		guard let url = URL(string: url) else {
			Swift.print("Invalid URL")
			return
		}
		
		let task = URLSession.shared.dataTask(with: url) { (data, response, error) in
			guard let data = data, error == nil else {
				Swift.print("Error fetching data: \(String(describing: error))")
				return
			}
			
			if let htmlString = String(data: data, encoding: .utf8) {
				DispatchQueue.main.async {
					self.webView.loadHTMLString(htmlString, baseURL: url)
				}
			}
		}
		task.resume()
	}
}
