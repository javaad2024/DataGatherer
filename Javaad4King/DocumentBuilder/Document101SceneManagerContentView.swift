//
//  Document101SceneManagerContentView.swift
//  Javaad4King
//
//  Created by AdvocatesClose on 5/26/24.
//

import SwiftUI

struct Document101SceneManagerContentView: View {
	@EnvironmentObject var sceneManager: SceneManager
	
	var body: some View {
		VStack {
			Text("Main View")
				.font(.largeTitle)
				.padding()
			
			Button("Launch New Scene") {
				sceneManager.addScene()
			}
			.padding()
		}
	}
}


#Preview {
    Document101SceneManagerContentView()
}
