//
//  SceneManager.swift
//  Javaad4King
//
//  Created by AdvocatesClose on 5/26/24.
//

import Foundation
import SwiftUI

class SceneManager: ObservableObject {
	@Published var scenes: [SceneData] = []
	
	func addScene() {
		let newScene = SceneData(id: UUID())
		scenes.append(newScene)
	}
}

struct SceneData: Identifiable {
	let id: UUID
}
