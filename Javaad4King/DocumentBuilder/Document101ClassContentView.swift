//
//  Document101ClassContentView.swift
//  Javaad4King
//
//  Created by AdvocatesClose on 5/26/24.
//

import SwiftUI

struct JDocumentApp101App: Scene {
	var body: some Scene {
		DocumentGroup(newDocument: DocumentApp101Document()) { file in
			JDocumentView(document: file.$document)
		}
	}
}

struct JDocumentView: View {
	@Binding var document: DocumentApp101Document
	
	init(document: Binding<DocumentApp101Document>) {
		self._document = document
	}
	
	var body: some View {
		TextEditor(text: $document.text)
	}
}

#Preview {
		//	let t = "HEY HEY"
	JDocumentView(document: .constant(DocumentApp101Document()))
}
