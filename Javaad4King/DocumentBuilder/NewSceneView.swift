//
//  NewSceneView.swift
//  Javaad4King
//
//  Created by AdvocatesClose on 5/26/24.
//

import SwiftUI

struct NewSceneView: View {
	var body: some View {
		Text("New Scene View")
			.font(.largeTitle)
			.padding()
	}
}


#Preview {
    NewSceneView()
}
