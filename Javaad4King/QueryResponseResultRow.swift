//
//  QueryResponseResultRow.swift
//  Javaad4King
//
//  Created by AdvocatesClose on 5/29/24.
//

import SwiftUI


struct QueryResponseResultRow: View {
	var queryResponse: QueryResponse
	
	var body: some View {
		HStack {
			VStack(alignment: .leading) {
				Text(queryResponse.companyName.uppercased())
					.font(.headline)
				Text(queryResponse.ticker)
					.font(.subheadline)
			}
			Spacer()
			VStack(alignment: .trailing) {
				Text("Form \(queryResponse.formType)")
					.font(.headline)
					.fontWeight(.bold)
				Text("Filed \(queryResponse.filedAt)")
					.font(.body)
			}
		}
    }
}


#Preview {
    QueryResponseResultRow(queryResponse: QueryResponse())
}
