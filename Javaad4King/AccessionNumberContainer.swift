//
//  AccessionNumberContainer.swift
//  Javaad4King
//
//  Created by AdvocatesClose on 5/24/24.
//

import Foundation


struct AccessionNumberContainer: Codable, Hashable {
	var directory: AccessionNumberDirectory = AccessionNumberDirectory()
}

struct AccessionNumberDirectory: Codable, Hashable {
	var items: [Item] = [Item]()
	var name: String = ""
	var parentDir: String = ""
	
	enum CodingKeys: String, CodingKey {
		case items = "item"
		case name
		case parentDir = "parent-dir"
	}
}

struct Item: Codable, Hashable {
	var lastModified: String = "[Last Modified]"
	var name: String = "[Name]"
	var size: String = "[Size]"
	var type: String = ""
	
	enum CodingKeys: String, CodingKey {
		case lastModified = "last-modified"
		case name
		case size
		case type
	}
}
