//
//  AccessionNumberContainerView.swift
//  Javaad4King
//
//  Created by AdvocatesClose on 5/24/24.
//

import SwiftUI

struct AccessionNumberContainerView: View {
	var queryResponse: QueryResponse
	
	@State private var accessionNumberContainer = AccessionNumberContainer()
	
	var body: some View {
		NavigationStack {
			VStack(alignment: .leading) {
				HStack {
					Text("Attachments")
						.font(.title2)
					Spacer()
				}
				ScrollView {
					ForEach(accessionNumberContainer.directory.items, id: \.name) { item in
						NavigationLink{
							WebView(url: "https://www.sec.gov/\(accessionNumberContainer.directory.name)/\(item.name)")
						} label: {
							makeAccessionNumberContainerItemRow(forItem: item)
						}
					}
				}
			}
			.padding(.vertical)
			Spacer()
		}
		.task {
			do {
				accessionNumberContainer = try await makeAccessionNumberContainer(result: queryResponse)
			} catch {
				print("Error in task at accession number container view")
			}
		}
	}

	
	func makeUrlToIndex(result res: QueryResponse) -> URL {
		let urlString = "https://www.sec.gov/Archives/edgar/data/\(res.cik)/\(res.accessionNo.replacingOccurrences(of: "-", with: ""))/index.json"
		let url = URL(string: urlString)!
		return url
	}
	
	func makeUrlRequest(url urlEndpoint: URL) -> URLRequest {
		var request = URLRequest(url: urlEndpoint)
		request.httpMethod = "POST"
		request.addValue("Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/17.5 Safari/605.1.15", forHTTPHeaderField: "User-Agent")
		let queryDict = ["User-Agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/17.5 Safari/605.1.15"]
		let jsonEncoder = JSONEncoder()
		jsonEncoder.outputFormatting = .prettyPrinted
		let jsonData = try? jsonEncoder.encode(queryDict)
		request.httpBody = jsonData
		return request
	}
	
	func makeAccessionNumberContainer(result res: QueryResponse) async throws -> AccessionNumberContainer {
		let urlToIndex = makeUrlToIndex(result: res)
		let request = makeUrlRequest(url: urlToIndex)
		
		let (_, _) = try await URLSession.shared.data(for: request)
		var accessionNumberContainer = AccessionNumberContainer()
		do {
			let (data, response) = try await URLSession.shared.data(from: urlToIndex)
			let container = try JSONDecoder().decode(AccessionNumberContainer.self, from: mapResponse(response: (data: data, response: response)))
			accessionNumberContainer = container
		} catch {
			print("There was an error at accessionNumberContainer")
		}
		return accessionNumberContainer
	}


	func makeAccessionNumberContainerItemRow(forItem item: Item) -> some View {
		HStack {
			VStack(alignment: .leading) {
				Text(item.type)
					.font(.headline)
				Text(item.name)
					.font(.subheadline)
			}
			Spacer()
			VStack(alignment: .trailing) {
				Text("Filed ")
					.font(.headline)
					.fontWeight(.bold)
			}
		}
		
	}
}

#Preview {
	AccessionNumberContainerView(queryResponse: QueryResponse())
}




