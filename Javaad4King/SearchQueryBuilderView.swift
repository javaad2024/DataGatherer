//
//  SearchView.swift
//  Javaad4King
//
//  Created by AdvocatesClose on 5/28/24.
//

import SwiftUI

struct QueryString: Identifiable, Hashable {
	
	let id = UUID()
	var companyName = "Company"
	var formType = "formType"
	var ticker = "Ticker"
}

enum SearchOptions: String, Identifiable, Hashable, CaseIterable {
	case companyName, formType, ticker
	var id: Self { self }
}


struct SearchQueryBuilderView: View {
	@State private var searchText = ""
	
	var body: some View {
		Form {
			Section {
				TextField("Company", text: $searchText)
					.clipped()
				
				
				
			}
		}
		.padding(10)
    }
}

#Preview {
    SearchQueryBuilderView()
}
