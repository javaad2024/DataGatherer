//
//  WebView.swift
//  Javaad4King
//
//  Created by AdvocatesClose on 5/26/24.
//
import SwiftUI
import Cocoa
import WebKit

struct WebView: NSViewRepresentable {
	var url: String
	
	func makeNSView(context: Context) -> WKWebView {
		guard let url = URL(string: url) else {
			return WKWebView()
		}
		let webview = WKWebView()
		webview.load(URLRequest(url: url))
		return webview
	}
	
	func updateNSView(_ nsView: WKWebView, context: Context) {
		guard let url = URL(string: url) else { return }
		
		let request = URLRequest(url: url)
		nsView.load(request)
	}
}


#Preview {
    WebView(url: "https://www.google.com/")
}
