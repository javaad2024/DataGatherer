//
//  RegistrationStatementContainer.swift
//  Javaad4King
//
//  Created by AdvocatesClose on 5/18/24.
//

import Foundation
import SwiftData


struct Total: Codable {
	var results: Int = 100
	var relation: String = "gte"
	
	enum CodingKeys: String, CodingKey {
		case results = "value"
		case relation
	}
}

struct RegistrationStatementsContainer: Codable {
	var total: Total = Total()
	var registrationStatements: [RegistrationStatement] = [RegistrationStatement]()
	
	
	enum CodingKeys: String, CodingKey {
		case total
		case registrationStatements = "data"
	}
}


//
//class QueryApiResponseClass {
//	let id = UUID()
//	var filedAt: String = ""
//	var accessionNo: String = "accessionNo"
//	var formType: String = "formType"
//	var cik: String = "cik"
//	var ticker: String = "ticker"
//	var filingUrl: String = "filingUrl"
//}
//



struct RegistrationStatement: Codable, Hashable, Identifiable {
	let id = UUID()
	var filedAt: String = ""
	var accessionNo: String = ""
	var formType: String = ""
	var cik: String = "cik"
	var ticker: String = "ticker"
	var filingUrl: String = "filingUrl"
	var entityName: String = "name"
	
	var underwriters: [Underwriter] = [Underwriter]()
	var lawFirms: [Lawyers] = [Lawyers]()
	var publicOfferingPrice: Proceeds = Proceeds()
	var underwritingDiscount: Proceeds = Proceeds()
	var proceedsBeforeExpenses: Proceeds = Proceeds()
	var employees: Employees = Employees()
	var management: [Management] = [Management]()
}

struct Underwriter: Codable, Hashable, Identifiable {
	let id = UUID()
	var name: String = "underwriter"
	
	private enum CodingKeys: String, CodingKey { case name }
}

struct Lawyers: Codable, Hashable, Identifiable {
	let id = UUID()
	var name: String = "Firm"
	var location: String = "City"
	
	private enum CodingKeys: String, CodingKey { case name, location }
}

struct Auditor: Codable, Hashable, Identifiable {
	let id = UUID()
	var name: String = ""
	
	private enum CodingKeys: String, CodingKey { case name }
}

struct Proceeds: Codable, Hashable, Identifiable {
	let id: UUID? = UUID()
	var perShare: Float? = 10.00
	var perShareText: String? = ""
	var total: Int? = 100
	var totalText: String? = ""
	
	private enum CodingKeys: String, CodingKey { case perShare, perShareText, total, totalText }
}

struct UnderwritingDiscount: Codable, Hashable {
		//	let id = UUID()
	var perShare = 10.00
	var perShareText: String = ""
	var total = 10.00
	var totalText: String = ""
	
	private enum CodingKeys: String, CodingKey { case perShare, perShareText, total, totalText }
}

struct ProceedsBeforeExpenses: Codable, Hashable, Identifiable {
	let id = UUID()
	var perShare: Float = 10.00
	var perShareText: String = ""
	var total: Float = 10.00
	var totalText: String = ""
	
	private enum CodingKeys: String, CodingKey { case perShare, perShareText, total, totalText }
}

struct Employees: Codable, Hashable, Identifiable {
	let id = UUID()
	var total: Int? = 190
	var asOfDate: String? = ""
	var perDivision: [Division?] = [Division]()
	var perRegion: [Region?] = [Region]()
	
	private enum CodingKeys: String, CodingKey { case total, asOfDate, perDivision, perRegion }
}

struct Management: Codable, Hashable, Identifiable {
	let id = UUID()
	var name: String? = "Person"
	var age: Int? = 50
	var position: String? = "CEO"
	
	private enum CodingKeys: String, CodingKey { case name, age, position }
}

struct Division: Codable, Hashable, Identifiable {
	let id = UUID()
	var division: String = ""
	var employees: Int = 100
	
	private enum CodingKeys: String, CodingKey { case division, employees }
}

struct Region: Codable, Hashable, Identifiable {
	let id = UUID()
	var region: String = ""
	var employees: Int = 100
	
	private enum CodingKeys: String, CodingKey { case region, employees }
}

