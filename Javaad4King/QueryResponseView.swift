//
//  QueryResponseContainerView.swift
//  Javaad4King
//
//  Created by AdvocatesClose on 5/22/24.
//

import SwiftUI


enum QueryToken: String, Identifiable, Hashable, CaseIterable {
	case accessionNo, formType, items, cik, ticker, companyName
	
	var id: Self { self }
}


struct QueryResponseView: View {
	@State private var searchText: String = ""
	@State private var queryResponseContainer = QueryResponseContainer()
	var queryResponses: [QueryResponse] {
		return queryResponseContainer.queryResponses
	}
	
	var body: some View {
		NavigationStack {
			QueryResponseRows(queryResponses: queryResponses)
				.searchable(text: $searchText)
		}
		.toolbar {
			Button(action: {
				Task {
					do {
						queryResponseContainer = try await fetchDataBySendingUrlRequest()
					} catch {
						print("NOPE 10101")
					}
				}
			}, label: {
				Text("Fetch Data")
			})
		}
	}
	
	
	
	func makeURLRequestForJansQueryApi() -> URLRequest {
		let urlToEndpoint = URL(string: "https://api.sec-api.io")!
		var request = URLRequest(url: urlToEndpoint)
		request.httpMethod = "POST"
		request.setValue("application/json", forHTTPHeaderField: "Content-Type")
		request.addValue("cba221f705b9631078b3cb797bfa90607d9ff2fc06c95a7a13c8bad01e1f22a0", forHTTPHeaderField: "Authorization")
		let queryDict = [
			"query": "formType:424*",
			"from": "0",
			"size": "50"
		]
		
		let jsonEncoder = JSONEncoder()
		jsonEncoder.outputFormatting = .prettyPrinted
		let jsonData = try? jsonEncoder.encode(queryDict)
		request.httpBody = jsonData
		return request
	}
	
	
	
	func fetchDataBySendingUrlRequest() async throws -> QueryResponseContainer {
		var container = QueryResponseContainer()
		let request = makeURLRequestForJansQueryApi()
		let (data, response) = try await URLSession.shared.data(for: request)
		do {
			container = try JSONDecoder().decode(QueryResponseContainer.self, from: mapResponse(response: (data: data, response: response)))
		} catch {
			print(data, response)
		}
		return container
	}
}




#Preview {
    QueryResponseView()
}

