# Javaadica-2

# Objective
Given an api, to be able to fetch data from such api. 

The following constraints exist. 
- Apis need to match interfaces but do not always follow consistent naming conventions. Will need middleware that
    - At a miniimum, should 
        - Create OpenAPI documentation for client, with such validation to be built at the level of the server
        - Provide methods to allow safe error-based validation. Errors should be fed back to the related method and solution should be by recursion.
    - Will eventually need to live in a container -> Linux
    - Builds *useful* documentation that is also *readable*

    - Finished product at this step must include a Dockerfile, all make files, any yml or plist files and all such other files as may be necessary. It is absolutely paramount that the machine should be able to find its way through an api and to create exactly one interface between the api and a middleware layer that:
        -  need not be the first and which will never be the last. 
        - Last layer should always be validation test, where such validation tests produce errors that permit, at a minimum:
            - machine readability
            - error correction, whether recursively or directly. ?* Error correction should be standardized as implementations of search problem.

